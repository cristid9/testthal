package testthales;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;

public class MainTest {

    private List<Integer> cams;

    @Before
    public void setCams() {
        cams = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9);
    }

    @Test
    public void testGroupSize3StepIncrement3() {
        List<List<Integer>> expected = Arrays.asList(
            Arrays.asList(1, 2, 3),
            Arrays.asList(4, 5, 6),
            Arrays.asList(7, 8, 9),
            Arrays.asList(1, 2, 3)
        );

        List<List<Integer>> actual = Main.groupCams(cams,3, 3);
        Exception hadExceptions = null;

        try {
            assertEquals(true, checkIdentical(expected, actual));
        } catch (Exception e) {
            hadExceptions = e;
        }

        // check that no exceptions are thrown from various reasons
        assertEquals(null, hadExceptions);
    }

    @Test
    public void testGroupSize3StepIncrement2() {
        List<List<Integer>> expected = Arrays.asList(
            Arrays.asList(1, 2, 3),
            Arrays.asList(3, 4, 5),
            Arrays.asList(5, 6, 7),
            Arrays.asList(7, 8, 9),
            Arrays.asList(9, 1, 2),
            Arrays.asList(2, 3, 4),
            Arrays.asList(4, 5, 6),
            Arrays.asList(6, 7, 8),
            Arrays.asList(8, 9, 1),
            Arrays.asList(1, 2, 3)
        );

        List<List<Integer>> actual = Main.groupCams(cams,3, 2);
        Exception hadExceptions = null;

        try {
            assertTrue(checkIdentical(expected, actual));
        } catch (Exception e) {
            hadExceptions = e;
        }

        // check that no exceptions are thrown from various reasons
        assertEquals(null, hadExceptions);
    }

    @Test
    public void testGroupSize3StepIncrement4() {
        List<List<Integer>> expected = Arrays.asList(
            Arrays.asList(1, 2, 3),
            Arrays.asList(5, 6, 7),
            Arrays.asList(9, 1, 2),
            Arrays.asList(4, 5, 6),
            Arrays.asList(8, 9, 1),
            Arrays.asList(3, 4, 5),
            Arrays.asList(7, 8, 9),
            Arrays.asList(2, 3, 4),
            Arrays.asList(6, 7, 8),
            Arrays.asList(1, 2, 3)
        );

        List<List<Integer>> actual = Main.groupCams(cams,3, 4);
        Exception hadExceptions = null;

        try {
            assertTrue(checkIdentical(expected, actual));
        } catch (Exception e) {
            hadExceptions = e;
        }

        // check that no exceptions are thrown from various reasons
        assertEquals(null, hadExceptions);
    }




    private boolean checkIdentical(List<List<Integer>> expected, List<List<Integer>> actual) {

        for (int i = 0; i < expected.size(); ++i) {
            for (int j = 0; j < expected.get(i).size(); ++j) {
                if (expected.get(i).get(j) != actual.get(i).get(j)) {
                    System.out.println(expected.get(i).get(j));
                    System.out.println(actual.get(i).get(j));
                    System.out.println(i + " " + j);
                    return false;
                }
            }
        }

        return true;
    }

}
