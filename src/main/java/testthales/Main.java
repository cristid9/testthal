package testthales;

import java.util.ArrayList;
import java.util.List;

public class Main {

    /**
     * Checks if the first and the last list in `groupedCame` are identical
     * @param groupedCams a list of lists of cams
     */
    private static boolean isDone(List<List<Integer>> groupedCams) {
        if (groupedCams.size() == 0 || groupedCams.size() == 1) { return false; }

        List<Integer> firstGroup = groupedCams.get(0);
        List<Integer> lastGroup = groupedCams.get(groupedCams.size() - 1);

        int i = 0, j = 0;
        while (i < firstGroup.size() && j < lastGroup.size()) {
            if (firstGroup.get(i) != lastGroup.get(j)) {
                return false;
            }
            i++;
            j++;
        }

        return true;
    }

     static final public  List<List<Integer>> groupCams(List<Integer> cams, int groupSize, int stepIncrement) {

        List<List<Integer>> groupedCams = new ArrayList<>();

        int currentGroupSize = 0;
        int lastResetIndex = 0;
        List<Integer> singleCamGroup = new ArrayList<>();

        int i = 0;

        while (i < cams.size() && !isDone(groupedCams)) {
            if (currentGroupSize == groupSize) {
                groupedCams.add(singleCamGroup);
                singleCamGroup = new ArrayList<>();
                currentGroupSize = 0;
                lastResetIndex = (lastResetIndex + stepIncrement) % cams.size();
                i = lastResetIndex;
            } else {
                singleCamGroup.add(cams.get(i));
                i = (i + 1) % cams.size();
                currentGroupSize++;
            }
        }

        return groupedCams;
    }

    public static void main(String[] args) {
        // check unit tests in `src/test/testthales/java/MainTest.java`
    }

}
